Task 1:
Joins
Using a LEFT JOIN, list all departments from the "departments" table and the count of employees in each department from the "employees" table.


Task 2 :
Joins:
Write a query that FULL OUTER JOINs the "employees" table with the "departments" table and displays the employee's first name,
last name, and department name.


Task  3:
Joins:
Write a query that INNER JOINs the "employees" table with the "jobs" table and displays the employee's first name, last name, and job title.


Task 4 :
Joins:
Write a query to display the first_name, last_ name, and department_name of all employees
who work in countries that have a country_name starting with 'U'.

Task 5:
Joins:
Write a query that RIGHT JOINs the "employees" table with the "departments" table and displays the employee's first name, last name, and department name.


Task 6:
Joins:
Write a query that joins the "employees" table and the "departments" table and displays the employee's first name, last name,
and department name, only for those departments that have more than 50 employees.
