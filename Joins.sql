--Iki ve daha cox cedveli birlesdirerek melumatlari elde etmek ucun istifade olunur.
--Bu emeliyyatlar verilenler bazasindan lazimli melumatlari effektiv sekilde cekmeye imkan verir
--Join tipler:
--Inner Join
--Left Join
--Right Join 
--Full Join
--Cross Join 
--Self Join
--Natural Join
--Using Clause Join


--Inner Join
--Her iki cedvelde uygun gelen setirleri qaytarir

SELECT a.column1, b.column2
FROM table1 a
INNER JOIN table2 b
ON a.common_column = b.common_column;

--Burada common_column yazmagimin meqsedi her iki table uygun sutunlari goturmekdir
--sutunlarin adlari ferqli ola biler,lakin icindeki datalarin tipleri ve megzleri eyni olmalidi

--Left Join
--Sol table daki butun setileri sag table da ise sola uygun gelenleri cixarir
--Eger sag cedvelde uygun gelen setir yoxdursa NULL cixarir

SELECT a.column1, b.column2
FROM table1 a
LEFT JOIN table2 b
ON a.common_column = b.common_column;

--Right Joinde bu mentiqle isleyir

--FULL Join (Full Outer Join)
--Hem sol hemde sag table daki butun setirleri qaytarir
--Eger her iki cedvelde uygun gelen setir yoxdursa Null qaytarir

SELECT a.column1, b.column2
FROM table1 a
FULL JOIN table2 b
ON a.common_column = b.common_column;

--Cross Join 
--Birinci cedveldeki her bir setiri ikinci cedveldeki her bir setir ile birlsedirir
--Neticede Kartezian hasilat alinir

SELECT a.column1, b.column2
FROM table1 a
FULL JOIN table2 b;

--Self Join 
--Bir cedvelin ozunu ozu ile birlesdirmek ucundu

SELECT a.column1 ,b.column2
FROM table1 a,table1 b 
WHERE a.common_column = b.common_column;

--Natural Join
--Adlari ve tipleri eyni olan sutunlari avtomatik olaraq birlesdirir

SELECT * FROM table1
Natural Join table2;

--USING Clause 
--bu sert yalniz her iki table eyni adda olan sutunlar ucun kecerlidir

SELECT a.column1 ,b.column2
FROM table1 a
INNER JOIN table2 b
USING (common_column);

